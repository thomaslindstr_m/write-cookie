// ---------------------------------------------------------------------------
//  write-cookie.js
// ---------------------------------------------------------------------------

    var STRING_EQUALS = '=';
    var STRING_DOT = '.';
    var STRING_SEMICOLON = ';';
    var STRING_SLASH = '/';

    var STRING_EXPIRES = 'Expires';
    var STRING_DOMAIN = 'Domain';
    var STRING_PATH = 'Path';
    var STRING_SAMESITE = 'SameSite';

    var plusOne = +1;
    var minusTwo = -2;

    /**
     * Write cookie
     * @param {string} name
     * @param {string} value
     * @param {object} options
     * @param {date} options.expiration
     * @param {string} options.domain
     * @param {string} options.path
     * @param {string} options.sameSite
     *
     * @example writeCookie('my_cookie', 'hello', {domain: 'www.mydomain.com'})
    **/
    function writeCookie(name, value, options) {
        var _options = options || {};

        var _cookie = [];
        _cookie.push(name + STRING_EQUALS + window.encodeURIComponent(value));

        var _expiration = null;

        if (_options.expiration
        && (_options.expiration instanceof Date)) {
            _expiration = _options.expiration;
        } else {
            _expiration = new Date();
            _expiration.setYear(new Date().getFullYear() + plusOne);
        }

        _cookie.push(STRING_EXPIRES + STRING_EQUALS + _expiration.toUTCString());

        var _domain = null;

        if (_options.domain) {
            _domain = _options.domain;
        } else {
            _domain = window.location.hostname.split(STRING_DOT).slice(minusTwo).join(STRING_DOT);
        }

        _cookie.push(STRING_DOMAIN + STRING_EQUALS + _domain);

        var _path = null;

        if (_options.path) {
            _path = _options.path;
        } else {
            _path = STRING_SLASH;
        }

        var _sameSite = null;

        if (_options.sameSite) {
            _sameSite = _options.sameSite;
        } else {
            _sameSite = 'Lax';
        }

        _cookie.push(STRING_SAMESITE + STRING_EQUALS + _sameSite);

        _cookie.push(STRING_PATH + STRING_EQUALS + _path);
        window.document.cookie = _cookie.join(STRING_SEMICOLON);
    }

    module.exports = writeCookie;
